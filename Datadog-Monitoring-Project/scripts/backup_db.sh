#!/bin/bash

# Variables for backup
DB_CONTAINER="db-server"
BACKUP_DIR="/path/to/backup"
DATE=$(date +%Y%m%d%H%M)

# Perform backup
echo "Backing up database..."
docker exec $DB_CONTAINER pg_dumpall -c -U postgres > "$BACKUP_DIR/db-backup-$DATE.sql"

echo "Database backup completed."
