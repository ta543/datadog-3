#!/bin/bash

# Path to the Docker directory
DOCKER_DIR="../docker"

# Building Docker images
echo "Building Docker images..."
docker build -t my-web-server "$DOCKER_DIR/web/"
docker build -t my-app-server "$DOCKER_DIR/app/"
docker build -t my-db-server "$DOCKER_DIR/db/"

# Running Docker containers
echo "Running Docker containers..."
docker run -d --name web-server -p 80:80 my-web-server
docker run -d --name app-server -p 3000:3000 my-app-server
docker run -d --name db-server -p 5432:5432 my-db-server

echo "Docker containers are deployed and running."
