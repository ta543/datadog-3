#!/bin/bash

# Starting Datadog agent on each Docker container
echo "Starting Datadog agent on web-server..."
docker exec web-server datadog-agent start

echo "Starting Datadog agent on app-server..."
docker exec app-server datadog-agent start

echo "Starting Datadog agent on db-server..."
docker exec db-server datadog-agent start

echo "Datadog agents started on all containers."
