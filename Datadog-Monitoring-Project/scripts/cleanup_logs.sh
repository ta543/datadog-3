#!/bin/bash

# Log directory path
LOG_DIR="../logs"

# Cleaning up old logs
echo "Cleaning up old logs..."
find "$LOG_DIR" -type f -name '*.log' -mtime +7 -exec rm {} \;

echo "Old logs cleaned up."
